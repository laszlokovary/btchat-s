package btchat.app.mobile.com.brainteam.btchat.messagemanager;

import java.util.Date;

/**
 * Created by lkovari on 2015.02.23..
 */
public class UserMessageItem {
    private String recipientUserId;
    private String userName;
    private String message;
    private Date sent;

    public UserMessageItem(String recipientUserId, String userName, String message, Date sent) {
        this.recipientUserId = recipientUserId;
        this.userName = userName;
        this.message = message;
        this.sent = sent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserMessageItem)) return false;

        UserMessageItem that = (UserMessageItem) o;

        if (!message.equals(that.message)) return false;
        if (!recipientUserId.equals(that.recipientUserId)) return false;
        if (!sent.equals(that.sent)) return false;
        if (!userName.equals(that.userName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = recipientUserId.hashCode();
        result = 31 * result + userName.hashCode();
        result = 31 * result + message.hashCode();
        result = 31 * result + sent.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "UserMessageItem{" +
                "recipientUserId='" + recipientUserId + '\'' +
                ", userName='" + userName + '\'' +
                ", message='" + message + '\'' +
                ", sent=" + sent +
                '}';
    }

    public String getRecipientUserId() {
        return recipientUserId;
    }

    public void setRecipientUserId(String recipientUserId) {
        this.recipientUserId = recipientUserId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getSent() {
        return sent;
    }

    public void setSent(Date sent) {
        this.sent = sent;
    }
}
