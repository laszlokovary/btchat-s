package btchat.app.mobile.com.brainteam.btchat.resthandling;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lkovari on 2/20/2015.
 */
public class UserInfoRESTHandler extends HttpGetRESTHandler {
    private String token_type;
    private String accessToken;

    /**
     *
     * @param hostAddress
     * @param hostPort
     * @param requestMode
     * @param httpRESTRequestDone
     * @param tokenType
     * @param accessToken
     */
    public UserInfoRESTHandler(String hostAddress, int hostPort, RequestMode requestMode, IHttpRESTRequestDone httpRESTRequestDone, String tokenType, String accessToken) {
        super(hostAddress, hostPort, requestMode, httpRESTRequestDone);
        this.accessToken = accessToken;
        this.token_type = tokenType;
    }

    /**
     *
     * @param params
     * @return
     */
    @Override
    protected Header[] createHeaders(String... params) {
        List<Header> headerList = new ArrayList<Header>();
        String token_type = params[0];
        String access_token = params[1];
        String param = token_type + " " + access_token;
        BasicHeader basicHeader = new BasicHeader("Authorization", param);
        headerList.add(basicHeader);
        return headerList.toArray(new Header[]{});
    }


    /**
     *
     * @param headerArray
     * @param url_path
     * @return
     */
    @Override
    protected String doIt(Header[] headerArray, String url_path) {
        Header[] currentHeaderArray = null;
        String currentUrlPath = null;

        if (headerArray != null)
            currentHeaderArray = headerArray;
        else
            currentHeaderArray = createHeaders(this.token_type, this.accessToken);

        if (url_path != null)
            currentUrlPath = url_path;
        else
            currentUrlPath = RESTPathDefinitions.URL_PATH_USERINFO;

        return super.doIt(currentHeaderArray, currentUrlPath);
    }

    @Override
    protected String doInBackground(String... params) {
        String response = null;
        BaseRESTHandler baseRESTHandler = null;
        try {
            response = doIt(null, null);
        } catch (Exception e) {
            response = null;
        }
        finally {
            baseRESTHandler = null;
        }
        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        String responseText = result;
        if (this.httpRESTRequestDone != null) {
            this.httpRESTRequestDone.requestDone(responseText);
        }
        super.onPostExecute(result);
    }


}
