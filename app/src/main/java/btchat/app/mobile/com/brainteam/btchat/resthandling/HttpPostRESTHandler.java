package btchat.app.mobile.com.brainteam.btchat.resthandling;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIUtils;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import btchat.app.mobile.com.brainteam.btchat.preference.BTCSettings;

/**
 * Created by lkovari on 2015.02.22..
 */
public abstract class HttpPostRESTHandler extends BaseRESTHandler {

    /**
     *
     * @param hostAddress
     * @param hostPort
     * @param requestMode
     * @param httpRESTRequestDone
     */
    protected HttpPostRESTHandler(String hostAddress, int hostPort, RequestMode requestMode, IHttpRESTRequestDone httpRESTRequestDone) {
        super(hostAddress, hostPort, requestMode, httpRESTRequestDone);
    }

    /**
     *
     * @param params
     * @return
     */
    protected abstract List<NameValuePair> createParams(String... params);

    /**
     *
     * @param parameters
     * @param url_path
     * @return
     */
    protected String doIt(List<NameValuePair> parameters, String url_path) {
        String responseText = null;
        try {
            initializeHost();
            // build URI
            URI uri = URIUtils.createURI("http", BTCSettings.HOST_ADDRESS, BTCSettings.HOST_PORT, url_path, null, null);
            HttpEntity httpEntity = new UrlEncodedFormEntity(parameters);
            this.httpRequest = new HttpPost(uri);
            ((HttpPost)this.httpRequest).setEntity(httpEntity);
            this.httpRequest.addHeader("Content-Type", "application/x-www-form-urlencoded");
            HttpResponse httpResponse = this.httpClient.execute(this.httpHost, this.httpRequest);
            StatusLine statusLine = httpResponse.getStatusLine();
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                responseText = inputStream2String(httpResponse.getEntity().getContent());
                System.out.println(responseText);
            }
            else {
                httpResponse.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            responseText = null;
        }
        finally {
            this.httpClient.getConnectionManager().shutdown();
            deinitializeHost();
        }
        return responseText;
    }

}
