package btchat.app.mobile.com.brainteam.btchat.json;

import java.util.Date;

/**
 * Created by lkovari on 2015.02.15..
 */
public class TokenJSonWrapper extends BaseJSonWrapper {
    private String user_id;
    private String token;
    private Date created;

    public TokenJSonWrapper(String user_id, String token, Date created) {
        this.user_id = user_id;
        this.token = token;
        this.created = created;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public String toJson() {
        return GSON.toJson(this, TokenJSonWrapper.class);
    }
}
