package btchat.app.mobile.com.brainteam.btchat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import btchat.app.mobile.com.brainteam.btchat.datawrappers.UserData;
import btchat.app.mobile.com.brainteam.btchat.displaymanager.ChatMessage;
import btchat.app.mobile.com.brainteam.btchat.displaymanager.ChatMessageDisplayManager;
import btchat.app.mobile.com.brainteam.btchat.displaymanager.MessageSideKind;
import btchat.app.mobile.com.brainteam.btchat.preference.BTCSettings;
import btchat.app.mobile.com.brainteam.btchat.preference.PreferenceSettings;
import btchat.app.mobile.com.brainteam.btchat.resthandling.IHttpRESTRequestDone;
import btchat.app.mobile.com.brainteam.btchat.resthandling.LoginRESTHandler;
import btchat.app.mobile.com.brainteam.btchat.resthandling.RequestMode;
import btchat.app.mobile.com.brainteam.btchat.utils.ConnectionUtils;
import btchat.app.mobile.com.brainteam.btchat.utils.StorageUtils;
import btchat.app.mobile.com.brainteam.btchat.utils.Utils;


import static btchat.app.mobile.com.brainteam.btchat.preference.BTCSettings.*;

/**
 * Created by lkovari on 2015.02.08..
 */
public class Chat extends Activity {
    private ChatMessageDisplayManager chatMessageDisplayManager;
    private String currentMessage;
    private EditText editText;
    private ImageButton sendButton;
    private String hostAddress;


    class LoginHttpRESTRequestDone implements IHttpRESTRequestDone {
        @Override
        public String requestDone(String response) {
            try {
                if (response != null) {
                    JSONObject jsonObject = new JSONObject(response);
                    BTCSettings.currentUserData.setAccess_token(jsonObject.getString("access_token"));
                    BTCSettings.currentUserData.setRefresh_token(jsonObject.getString("refresh_token"));
                    BTCSettings.currentUserData.setExpires_in(jsonObject.getInt("expires_in"));
                    BTCSettings.currentUserData.setToken_type(jsonObject.getString("token_type"));

                    String title = getActionBar().getTitle().toString();
                    getActionBar().setTitle(title + " " + getString(R.string.chat_logged_in) + " " + BTCSettings.currentUserData.getUserName());
                    // write last logged user into preference
                    Utils.writeLastLoggedUser(Chat.this, BTCSettings.currentUserData.getUserName());
                }
                else {
                    String err_mess = getString(R.string.error_login_failed_for) + " " + BTCSettings.USER_NAME;
                    Toast.makeText(getApplicationContext(), err_mess , Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        // load preferences
        loadPreferences(getApplicationContext());

        this.hostAddress = HOST_ADDRESS;

        this.chatMessageDisplayManager = new ChatMessageDisplayManager(this, R.id.discussionListView, null, hostAddress);

        this.editText = (EditText) findViewById(R.id.editDiscussionText);

        editText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    textView.setText(textView.getText() + "\n");
                    return true;
                }
                return false;
            }
        });

        this.sendButton = (ImageButton)findViewById(R.id.sendImageButton);

        this.sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentMessage = editText.getText().toString();

                MessageSideKind currentSideKind = chatMessageDisplayManager.makeOppositeMessageSideKind();

                Bitmap avatar = null;
                switch (currentSideKind) {
                    case LEFT: {
                        avatar = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.np_face_left);
                        break;
                    }

                    case RIGHT: {
                        avatar = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.np_face_right);
                        break;
                    }
                }
//TODO set user id
                String userId = "2";
                ChatMessage chatMessage = new ChatMessage(userId, currentUserData.getUserName(), new Date(), avatar, currentMessage, currentSideKind);

                chatMessageDisplayManager.sendMessage(chatMessage);

                editText.setText("");

            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();

        // read last logged user
        String lastLoggedUserName = Utils.readLastLoggedUser(Chat.this);
        if (lastLoggedUserName != null) {
            // user name in preference settings is same than the last logged user?
            if (lastLoggedUserName.equals(BTCSettings.USER_NAME)) {
                BTCSettings.currentUserData = new UserData(BTCSettings.USER_NAME, BTCSettings.USER_PASSWORD);
            }
            else {
                // now need to show login dialog
                String[] un_pw = ShowLoginDialog.showLoginDialog(Chat.this, lastLoggedUserName);
                BTCSettings.USER_NAME = un_pw[0];
                BTCSettings.USER_PASSWORD = un_pw[1];
            }
        }
        else {
            if (BTCSettings.USER_NAME == null) {
//TODO need to create a user
                BTCSettings.USER_NAME = "andrey";
                BTCSettings.USER_PASSWORD = "simplepassword";
            }
        }
        // login
        BTCSettings.currentUserData = new UserData(BTCSettings.USER_NAME, BTCSettings.USER_PASSWORD);
        LoginHttpRESTRequestDone loginHttpRESTRequestDone = new LoginHttpRESTRequestDone();
        LoginRESTHandler loginRESTHandler = new LoginRESTHandler(BTCSettings.HOST_ADDRESS, BTCSettings.HOST_PORT, RequestMode.HTTP_POST, loginHttpRESTRequestDone, currentUserData.getUserName(), currentUserData.getPassword());
        loginRESTHandler.execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        exit();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        int id = item.getItemId();

        Intent intent = null;
        switch (id) {
            case R.id.btc_itemAbout : {
                Toast.makeText(getApplicationContext(), "Not implemented yet!", Toast.LENGTH_LONG).show();
                break;
            }
            case R.id.btc_itemSettings : {
                intent = new Intent();
                intent.setClass(getApplicationContext(), PreferenceSettings.class);
                startActivity(intent);
                return true;
            }
            case R.id.btc_itemHelp : {
                Toast.makeText(getApplicationContext(), "Not implemented yet!", Toast.LENGTH_LONG).show();
                /*
                if (ConnectionUtils.isInternetAvailable(getCurrentActivity())) {
                    intent = new Intent();
                    //12/23/2012
                    intent.setClass(getApplicationContext(), BTCHelp.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(this, R.string.error_nointernetconnectionavailable, Toast.LENGTH_LONG).show();
                }
                */
                return true;
            }
            case R.id.btc_userprofile : {
                if (BTCSettings.currentUserData.getAccess_token() != null) {
                    intent = new Intent();
                    intent.setClass(getApplicationContext(), UserInfo.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(this, R.string.error_nointernetconnectionavailable, Toast.LENGTH_LONG).show();
                }
                break;
            }
            case R.id.btc_itemExit : {
                if (StorageUtils.isStoreDebugLog) {
                    StorageUtils.createFileForStringBuffer(this.getApplicationContext(), "BTC-Debuglog.txt", StorageUtils.log);
                }

                exit();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    private void exit() {
        Intent intentData = new Intent(Intent.ACTION_MAIN);
        /*
        intentData.putExtra("IS_EXIT", new Boolean(true));
        intentData.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        setResult(Activity.RESULT_CLOSE_ALL);
        finish();
        System.exit(0);
        */
        intentData.addCategory(Intent.CATEGORY_HOME);
        intentData.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentData.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intentData);
        finish();
        System.exit(1);
    }

    @Override
    public void onBackPressed() {
        exit();
        this.onBackPressed();
    }

    /**
     *
     * @param isExitWhenPressOk boolean - if true exit when press ok
     */
    public void checkMobileDataConnection(final boolean isExitWhenPressOk) {
        // check in settings the option Use GPS or Wireless Network
        Context context = getApplicationContext();
        boolean isInternetEnabled = ConnectionUtils.isInternetAvailable(Chat.this);
        if (!isInternetEnabled) {
            String mess = getApplicationContext().getString(R.string.error_nointernetconnectionavailable);
            String title = getApplicationContext().getString(R.string.error_nointernetconnectionavailable_title);
            //AlertDialog alertDialog = new AlertDialog.Builder(Main.this).create();
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle(title);
            alertDialog.setMessage(mess);
            alertDialog.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog,int which) {
                    if (isExitWhenPressOk) {
                        dialog.cancel();
                        exit();
                    }
                }
            });
            alertDialog.show();
        }
    }

}
