package btchat.app.mobile.com.brainteam.btchat.datawrappers;

/**
 * Created by lkovari on 2/20/2015.
 */
public class UserInfoData {
    private String user_id;
    private String name;
    private String scope;

    public UserInfoData(String user_id, String name, String scope) {
        this.user_id = user_id;
        this.name = name;
        this.scope = scope;
    }

    public String getUser_id() {
        return user_id;
    }

    @Override
    public String toString() {
        return "UserInfoData{" +
                "user_id='" + user_id + '\'' +
                ", name='" + name + '\'' +
                ", scope='" + scope + '\'' +
                '}';
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
