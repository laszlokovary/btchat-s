package btchat.app.mobile.com.brainteam.btchat.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by lkovari on 2015.02.15..
 */
public class BaseJSonWrapper {
    protected static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    public String toJson() {
        return GSON.toJson(this, BaseJSonWrapper.class);
    }

    public Class fromJSon(String jsonInput) {
        return null;
    }

}
