package btchat.app.mobile.com.brainteam.btchat.displaymanager;

import android.graphics.Bitmap;
import android.media.Image;

import java.util.Date;

/**
 * Created by lkovari on 2015.02.08..
 */
public class ChatMessage {
    private String userId;
    private String displayName;
    private Date timeStamp;
    private Bitmap faceImage;
    private String message;
    private MessageSideKind messageSideKind = MessageSideKind.RIGHT;

    /**
     *
     * @param displayName String - display name
     * @param timeStamp Date - time stamp
     * @param faceImage Bitmap - picture of sender
     * @param message String - message
     * @param messageSideKind MessageSideKind - which part have to display the message, LEFT or RIGHT
     */
    public ChatMessage(String userId, String displayName, Date timeStamp, Bitmap faceImage, String message, MessageSideKind messageSideKind) {
        this.userId = userId;
        this.displayName = displayName;
        this.timeStamp = timeStamp;
        this.faceImage = faceImage;
        this.message = message;
        this.messageSideKind = messageSideKind;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Date getTimeStamp() {
        return this.timeStamp;
    }

    public Bitmap getFaceImage() {
        return faceImage;
    }

    public String getMessage() {
        return this.message;
    }


    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setFaceImage(Bitmap faceImage) {
        this.faceImage = faceImage;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MessageSideKind getMessageSideKind() {
        return messageSideKind;
    }

    public void setMessageSideKind(MessageSideKind messageSideKind) {
        this.messageSideKind = messageSideKind;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChatMessage)) return false;

        ChatMessage that = (ChatMessage) o;

        if (displayName != null ? !displayName.equals(that.displayName) : that.displayName != null)
            return false;
        if (message != null ? !message.equals(that.message) : that.message != null) return false;
        if (timeStamp != null ? !timeStamp.equals(that.timeStamp) : that.timeStamp != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 13;
        result = 31 + result + ( displayName != null ? displayName.hashCode() : 0);
        result = 31 * result + (timeStamp != null ? timeStamp.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }
}
