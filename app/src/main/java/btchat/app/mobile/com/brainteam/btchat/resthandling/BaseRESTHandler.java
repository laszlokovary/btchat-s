package btchat.app.mobile.com.brainteam.btchat.resthandling;

import android.os.AsyncTask;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by lkovari on 2/16/2015.
 */
abstract class BaseRESTHandler extends AsyncTask<String, Void, String> {
    protected String request;
    protected String hostAddress;
    protected int hostPort;
    protected HttpClient httpClient;
    protected HttpHost httpHost;
    protected RequestMode requestMode;
    protected HttpRequestBase httpRequest;

    protected IHttpRESTRequestDone httpRESTRequestDone;

    /**
     *
     * @param hostAddress String - host address
     */
    public BaseRESTHandler(String hostAddress, int hostPort, RequestMode requestMode, IHttpRESTRequestDone httpRESTRequestDone) {
        this.hostAddress = hostAddress;
        this.hostPort = hostPort;
        this.requestMode = requestMode;
        this.httpRESTRequestDone = httpRESTRequestDone;
    }

    protected void initializeHost() {
        this.httpClient = new DefaultHttpClient();
        this.httpHost = new HttpHost(this.hostAddress, this.hostPort, "http");
    }


    protected void deinitializeHost() {
        this.httpClient = null;
        this.httpHost = null;
    }

    /**
     *
     * @param inputStream InputStream - the content
     * @return String - content in text
     */
    protected String inputStream2String(java.io.InputStream inputStream) {
        String responseText = null;
        BufferedReader bufferedReader = null;
        StringBuilder inputStringBuilder = null;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String line = bufferedReader.readLine();
            inputStringBuilder = new StringBuilder();
            while (line != null) {
                inputStringBuilder.append(line);
                inputStringBuilder.append("\n");
                line = bufferedReader.readLine();
            }
            responseText = inputStringBuilder.toString();
        }
        catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            bufferedReader = null;
            inputStringBuilder = null;
        }
        return responseText;
    }

    @Override
    protected String doInBackground(String... params) {
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }
}
