package btchat.app.mobile.com.brainteam.btchat;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import btchat.app.mobile.com.brainteam.btchat.preference.BTCSettings;
import btchat.app.mobile.com.brainteam.btchat.resthandling.IHttpRESTRequestDone;
import btchat.app.mobile.com.brainteam.btchat.resthandling.RequestMode;
import btchat.app.mobile.com.brainteam.btchat.resthandling.UserInfoRESTHandler;
import btchat.app.mobile.com.brainteam.btchat.datawrappers.UserInfoData;
import btchat.app.mobile.com.brainteam.btchat.resthandling.UsersRESTHandler;


public class UserInfo extends Activity {
    private UserInfoData userInfo;
    private TextView userIdTextView;
    private TextView emailTextView;
    private Spinner contrySpinner;
    private Spinner stateSpinner;
    private TextView addressEditText;
    private EditText dobEditText;
    private TextView description;
    private Button saveButton;

    /**
     *  Update UI
     */
    class UserInfoHttpRESTRequestDone implements IHttpRESTRequestDone {
        @Override
        public String requestDone(String response) {
            try {
                if ((response != null) && (!response.equals("Unauthorized"))) {
                    JSONObject jsonObject = new JSONObject(response);
                    userInfo.setUser_id(jsonObject.getString("user_id"));
                    userInfo.setName(jsonObject.getString("name"));
                    userInfo.setScope(jsonObject.getString("scope"));

                    if (userIdTextView != null) {
                        userIdTextView.setText(userInfo.getUser_id() + " (" + userInfo.getName() + ")");
                    }
                    System.out.println(userInfo.toString());
                }
                else {
                    String err_mess = getString(R.string.error_userinfo_user_not_logged_in) + " " + response;
                    Toast.makeText(getApplicationContext(), err_mess, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        this.userIdTextView = (TextView)findViewById(R.id.userinfo_userid_value_textview);
        this.emailTextView = (EditText)findViewById(R.id.userinfo_email_edittext);
        this.contrySpinner = (Spinner)findViewById(R.id.userinfo_country_spinner);
        this.stateSpinner = (Spinner)findViewById(R.id.userinfo_state_spinner);
        this.addressEditText = (EditText)findViewById(R.id.userinfo_address_edittext);
        this.dobEditText  = (EditText)findViewById(R.id.userinfo_dob_edittext);
        this.description = (EditText)findViewById(R.id.userinfo_description_edittext);
        this.saveButton = (Button)findViewById(R.id.userinfo_submit_button);

        if (this.saveButton != null) {
            View.OnClickListener buttonListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            };
            this.saveButton.setOnClickListener(buttonListener);
        }

        if (BTCSettings.currentUserData.getAccess_token() != null) {
            UserInfoHttpRESTRequestDone userInfoHttpRESTRequestDone = new UserInfoHttpRESTRequestDone();
            this.userInfo = new UserInfoData(null, null, null);
            UserInfoRESTHandler userInfoRESTHandler = new UserInfoRESTHandler(BTCSettings.HOST_ADDRESS, BTCSettings.HOST_PORT, RequestMode.HTTP_GET, userInfoHttpRESTRequestDone, BTCSettings.currentUserData.getToken_type(), BTCSettings.currentUserData.getAccess_token());
            userInfoRESTHandler.execute();

            // test users
            UsersRESTHandler usersRESTHandler = new UsersRESTHandler(BTCSettings.HOST_ADDRESS, BTCSettings.HOST_PORT, RequestMode.HTTP_GET, null, BTCSettings.currentUserData.getToken_type(), BTCSettings.currentUserData.getAccess_token());
            usersRESTHandler.execute();

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
