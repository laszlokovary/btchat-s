package btchat.app.mobile.com.brainteam.btchat.resthandling;

/**
 * Created by lkovari on 2/16/2015.
 */
public enum RequestMode {
    HTTP_GET,
    HTTP_POST;
}
