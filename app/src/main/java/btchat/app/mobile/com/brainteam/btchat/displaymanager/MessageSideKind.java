package btchat.app.mobile.com.brainteam.btchat.displaymanager;

public enum MessageSideKind {
    LEFT,
    RIGHT;
}