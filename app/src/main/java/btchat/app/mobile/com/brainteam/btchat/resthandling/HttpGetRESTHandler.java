package btchat.app.mobile.com.brainteam.btchat.resthandling;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIUtils;

import java.net.URI;

import btchat.app.mobile.com.brainteam.btchat.preference.BTCSettings;

/**
 * Created by lkovari on 2015.02.22..
 */
public abstract class HttpGetRESTHandler extends BaseRESTHandler {

    /**
     * @param hostAddress         String - host address
     * @param hostPort
     * @param requestMode
     * @param httpRESTRequestDone
     */
    public HttpGetRESTHandler(String hostAddress, int hostPort, RequestMode requestMode, IHttpRESTRequestDone httpRESTRequestDone) {
        super(hostAddress, hostPort, requestMode, httpRESTRequestDone);
    }

    /**
     *
     * @param params
     * @return
     */
    protected abstract Header[] createHeaders(String... params);

    /**
     *
     * @param headerArray
     * @param url_path
     * @return
     */
    protected String doIt(Header[] headerArray, String url_path) {
        String responseText = null;
        try {
            initializeHost();
            URI uri = URIUtils.createURI("http", BTCSettings.HOST_ADDRESS, BTCSettings.HOST_PORT, url_path, null, null);
            this.httpRequest = new HttpGet(uri);
            this.httpRequest.setHeaders(headerArray);
            HttpResponse httpResponse = this.httpClient.execute(this.httpHost, this.httpRequest);
            StatusLine statusLine = httpResponse.getStatusLine();
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                responseText = inputStream2String(httpResponse.getEntity().getContent());
                System.out.println(responseText);
            }
            else {
                httpResponse.getEntity().getContent().close();
                responseText = statusLine.getReasonPhrase();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            this.httpClient.getConnectionManager().shutdown();
            deinitializeHost();
        }
        return responseText;
    }
}
