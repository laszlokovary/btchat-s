package btchat.app.mobile.com.brainteam.btchat.json;

/**
 * Created by lkovari on 2015.02.15..
 */
public class UserInfoJSonWrapper extends BaseJSonWrapper {
    private String user_id;
    private String email;
    private String country;
    private String state;
    private String address;
    private int year_of_birth;
    private String description;

    public UserInfoJSonWrapper(String user_id, String email, String country, String state, String address, int year_of_birth, String description) {
        this.user_id = user_id;
        this.email = email;
        this.country = country;
        this.state = state;
        this.address = address;
        this.year_of_birth = year_of_birth;
        this.description = description;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getYear_of_birth() {
        return year_of_birth;
    }

    public void setYear_of_birth(int year_of_birth) {
        this.year_of_birth = year_of_birth;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toJson() {
        return GSON.toJson(this, UserInfoJSonWrapper.class);
    }

}
