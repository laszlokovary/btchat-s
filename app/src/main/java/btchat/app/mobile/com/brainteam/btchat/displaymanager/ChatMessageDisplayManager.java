package btchat.app.mobile.com.brainteam.btchat.displaymanager;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.widget.ListView;

import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import btchat.app.mobile.com.brainteam.btchat.json.UserJSonWrapper;

/**
 * Created by lkovari on 2015.02.08..
 */
public class ChatMessageDisplayManager {
    private List<ChatMessage> chatMessages = new ArrayList<ChatMessage>();
    private ChatMessageListAdapter chatMessageListAdapter;
    private ListView listView;
    private ChatMessage lastChatMessage;
    private Handler handler = new Handler();
    private MessageSideKind lastMessageSideKind = MessageSideKind.LEFT;
    private WebSocketManager webSocketManager;
    private String currentUserId;

    /**
     *
     */
    class WebSocketManager extends WebSocketClient {

        WebSocketManager(URI serverURI) {
            super(serverURI);
        }

        @Override
        public void onOpen(ServerHandshake handshakedata) {
            Log.i("WebSocket", "onOpen");
            send("Open " + Build.MANUFACTURER + " " + Build.MODEL);
        }

        @Override
        public void onMessage(String message) {
            Log.i("WebSocket", "onMessage " + message);
//TODO set userId
            String userId = "1";
            ChatMessage chatMessage = new ChatMessage(userId, Build.MODEL, new Date(), null, message, makeOppositeMessageSideKind());
            receiveMessage(chatMessage);
        }

        @Override
        public void onClose(int code, String reason, boolean remote) {
            Log.i("WebSocket", "onClosed " + reason);
        }

        @Override
        public void onError(Exception ex) {
            Log.i("WebSocket", "onError " + ex.getMessage());
        }

    }

    public void connectWebSocket(String hostAddress) {
        URI uri;
        try {
            uri = new URI(hostAddress);
            this.webSocketManager = new WebSocketManager(uri);
            this.webSocketManager.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param activity Activity - owner activity
     * @param lvResourceId int - resource ID of ListView
     * @param chatMessages List<ChatMessage> - chat messages
     * @param hostAddress String - server host address
     */
    public ChatMessageDisplayManager(Activity activity, int lvResourceId, List<ChatMessage> chatMessages, String hostAddress) {
        if ((chatMessages != null) && (chatMessages.size() > 0)) {
            this.chatMessages.clear();
            this.chatMessages.addAll(chatMessages);
        }

        this.chatMessageListAdapter = new ChatMessageListAdapter(activity.getApplicationContext(), lvResourceId, this.chatMessages);

        this.listView =  (ListView) activity.findViewById(lvResourceId);

        this.listView.setAdapter(this.chatMessageListAdapter);
        this.listView.invalidateViews();

        UserJSonWrapper userJSonWrapper = new UserJSonWrapper("458976ö34976958764", "lkovary", "password", "salt");
        String jsonText = userJSonWrapper.toJson();
        Log.d("D", jsonText);

        connectWebSocket(hostAddress);
    }

    /**
     *
     * @return List<ChatMessage> - return the list of ChatMessages
     */
    public List<ChatMessage> getChatMessages() {
        return chatMessages;
    }

    /**
     *
     * @return MessageSideKind - return the last used MessageSideKind
     */
    public MessageSideKind getLastMessageSideKind() {
        return lastMessageSideKind;
    }

    /**
     *
     * @return MessageSideKind -  LEFT or RIGHT part of the message line
     */
    public MessageSideKind makeOppositeMessageSideKind() {
        lastMessageSideKind = lastMessageSideKind.equals(MessageSideKind.LEFT) ? MessageSideKind.RIGHT : MessageSideKind.LEFT;
        return lastMessageSideKind;
    }

    /**
     *
     * @param messages List<ChatMessage> - chat messages
     */
    public void setChatMessages(List<ChatMessage> messages) {
        this.chatMessages.clear();
        this.chatMessages.addAll(messages);
    }

    /**
     *
     * @param chatMessage ChatMessage -  called when receive a message
     */
    public void receiveMessage(ChatMessage chatMessage) {
        this.lastChatMessage = chatMessage;
        chatMessages.add(lastChatMessage);
        chatMessageListAdapter.notifyDataSetChanged();
    }

    /**
     *
     * @param chatMessage ChatMessage -  send a chat message
     */
    public void sendMessage(ChatMessage chatMessage) {
        this.lastChatMessage = chatMessage;
        chatMessages.add(lastChatMessage);
        chatMessageListAdapter.notifyDataSetChanged();
        // is connection open?
        WebSocket webSocket = webSocketManager.getConnection();
        if ((webSocket != null) && (webSocket.isOpen())) {
            webSocketManager.send(chatMessage.getMessage());
        }
    }
}
