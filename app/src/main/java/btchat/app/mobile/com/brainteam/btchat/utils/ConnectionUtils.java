package btchat.app.mobile.com.brainteam.btchat.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build.VERSION;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

/**
 * Created by lkovari on 2015.02.08..
 */
public class ConnectionUtils {
    private static String WEBSITE_LOCATION_OF_IMEIS = "http://www.brainteam.com/files/phoneids.txt";

    /**
     *
     * @param activity
     * @return
     */
    public static boolean isInternetAvailable(Activity activity) {
        boolean isInternetAvailable = false;
        boolean isMobileTypeConnected = isMobile3GDataConnectionAvailable(activity);
        boolean isWifiTypeConnected = isWiFiDataConnectionAvailable(activity);
        // one of Mobile or Wifi is connected?
        isInternetAvailable = (isMobileTypeConnected || isWifiTypeConnected);
        return isInternetAvailable;
    }

    /**
     *
     * @param activity - Activity of application
     * @return true if WiFi is connecting or connected
     */
    public static boolean isWiFiDataConnectionAvailable(Activity activity) {
        boolean isWifiTypeConnected = false;
        // get ConnectivityManager
        ConnectivityManager connectivityManager = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        // get NetworkInfo with TYPE_WIFI
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if ((networkInfo != null)  && (networkInfo.isAvailable()) && (networkInfo.isConnected())) {
            isWifiTypeConnected = networkInfo.getState() == NetworkInfo.State.CONNECTED;
        }
        return isWifiTypeConnected;
    }

    /**
     *
     * @param activity - Activity of application
     * @return true if Mobile is connecting or connected
     */
    public static boolean isMobile3GDataConnectionAvailable(Activity activity) {
        boolean isMobileTypeConnected = false;
        // get ConnectivityManager
        ConnectivityManager connectivityManager = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        // get NetworkInfo with TYPE_MOBILE
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((networkInfo != null)  && (networkInfo.isAvailable()) && (networkInfo.isConnected())) {
            isMobileTypeConnected = networkInfo.getState() == NetworkInfo.State.CONNECTED;
        }
        return isMobileTypeConnected;
    }


    /**
     *
     * @param activity Activity - an interested activity
     * @return Boolean - true if WiFi is enabled
     */
    public static boolean isWiFiEnabled(Activity activity) {
        WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        return wifiManager.isWifiEnabled();
    }

    /**
     *
     * @param activity Activity - an interested activity
     */
    public static void turnOnWiFi(Activity activity) {
        WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        if(wifiManager.isWifiEnabled()){
            // nothing to do
        }else{
            wifiManager.setWifiEnabled(true);
        }
    }

    /**
     *
     * @param activity Activity - an interested activity
     */
    public static void turnOffWiFi(Activity activity) {
        WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
        if(wifiManager.isWifiEnabled()){
            wifiManager.setWifiEnabled(false);
        }else{
            // nothing to do
        }
    }

    /**
     *
     * @param context Context - the current context
     * @return Boolean - true if enabled
     */
    public static boolean isWirelessNetworkEnabled(Context context) {
        boolean isWirelessNetwork = false;
        // check version
        if (VERSION.SDK_INT < 8) {
            ContentResolver cr = context.getContentResolver();
            String enabledProviders = Settings.Secure.getString(cr, Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (!TextUtils.isEmpty(enabledProviders)) {
                // not the fastest way to do that :)
                String[] providersList = TextUtils.split(enabledProviders, ",");
                for (String provider : providersList) {
                    if (LocationManager.NETWORK_PROVIDER.equals(provider)) {
                        isWirelessNetwork = true;
                        break;
                    }
                }
            }
        }
        else {
            ContentResolver contentResolver = context.getContentResolver();
            isWirelessNetwork = Settings.Secure.isLocationProviderEnabled(contentResolver, LocationManager.NETWORK_PROVIDER);
        }
        return isWirelessNetwork;
    }


    /**
     * @param context Context - the current context
     * @return Boolean - true if mobile network data is enabled
     */
    public static boolean isMobileNetworkDataEnabled(Context context) {
        boolean isDataEnabled = false;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
            isDataEnabled = (netInfo != null && netInfo.isConnectedOrConnecting());
        }
        catch (Exception e) {
            isDataEnabled = false;

        }
        return isDataEnabled;
    }

    /**
     *
     * @param context Context - the current context
     * @param isEnable Boolean - true if enabled
     */
    public static void setupUseWirelessNetwordSettings(Context context, boolean isEnable) {
        ContentResolver contentResolver = context.getContentResolver();
        Settings.Secure.setLocationProviderEnabled(contentResolver, LocationManager.NETWORK_PROVIDER, isEnable);
    }


    /**
     * @param activity Activity - an interested activity
     * @return String - IMEI number of phone
     */
    public static String extractPhoneIMEI(Activity activity) {
        String phoneIMEI = null;
        TelephonyManager telephonyManager = (TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager != null) {
            phoneIMEI = telephonyManager.getDeviceId();
        }
        telephonyManager = null;
        return phoneIMEI;
    }

    /**
     * @return String - IMEI numbers which has extra functionatily
     * @throws Exception
     */
    public static String downloadIMEINumbers() throws Exception {
        StringBuilder imeiNumbers = new StringBuilder();
        String imei = null;
        String imeis = null;
        try {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(WEBSITE_LOCATION_OF_IMEIS);
            HttpResponse httpResponse = httpclient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            BufferedHttpEntity bufferedHttpEntity = new BufferedHttpEntity(httpEntity);
            InputStream inputStream = bufferedHttpEntity.getContent();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            while ((imei = bufferedReader.readLine()) != null) {
                imeiNumbers.append(imei + "\n");
            }
            bufferedReader.close();
            imeis = imeiNumbers.toString();
        }
        catch (Exception e) {
            imeis = null;
        }
        imeiNumbers = null;
        return imeis;
    }

    /**
     * @param text String - input text
     * @return String - string in MD5
     * @throws Exception
     */
    public static String md5(String text) throws Exception {
        String digest = null;
        // Create MD5 Hash
        MessageDigest messageDigest = java.security.MessageDigest.getInstance("MD5");
        messageDigest.update(text.getBytes());
        byte messageDigestBytes[] = messageDigest.digest();

        // Create Hex String
        StringBuffer hexString = new StringBuffer();
        for (int i=0; i < messageDigestBytes.length; i++) {
            hexString.append(Integer.toHexString(0xFF & messageDigestBytes[i]));
        }
        digest = hexString.toString();
        return digest;
    }
}
