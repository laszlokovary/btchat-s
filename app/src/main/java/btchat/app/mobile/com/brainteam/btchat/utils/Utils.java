package btchat.app.mobile.com.brainteam.btchat.utils;

import java.lang.reflect.Field;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;



/**
 * Created by lkovari on 2015.02.08..
 */
public class Utils {
    public static final String PREFS_NAME = "wscipr";
    public static final String LICENSE_PREFERENCE_TAG = "islicenseaccepted";
    public static final String PREFS_LAST_LOGGED_USER = "lstlgdusr";
    public static final String LOG_FILE_PATH_TAG = "logfilepath";



    /**
     *
     * @param c Class - interested class
     * @param string String - enum string
     * @return Enum - desired anum
     */
    public static <T extends Enum<T>> T getEnumByString(Class<T> c, String string) {
        if( c != null && string != null ) {
            try {
                return Enum.valueOf(c, string);
            }
            catch(IllegalArgumentException ex) {
                throw new RuntimeException("Can't get Enum by String with valueOf!");
            }
        }
        return null;
    }

    /**
     * @param activity Activity - pass an activity
     * @return String - formated app version text
     */
    public static String extractVersionText(Activity activity) {
        String versionName = "";
        String versionCode = "";
        PackageInfo packageInfo;
        try {
            packageInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            versionName = "v " + packageInfo.versionName;
            versionCode = " #" + packageInfo.versionCode;

        } catch (NameNotFoundException e) {
            versionName = "-";
            versionCode = "-";
            e.printStackTrace();
        }
        return versionCode + " " + versionName;
    }

    /**
     *
     * @param activity Activity - interested activity
     * @return Boolean - if true accepted else false not accepted
     */
    public static boolean readLicenseAccepted(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, 0);
        boolean isLicenseAccepted = settings.getBoolean(LICENSE_PREFERENCE_TAG, false);
        return isLicenseAccepted;
    }

    /**
     *
     * @param activity Activity - interested activity
     * @param isAccepted Boolean - true if accepted, else false
     */
    public static void writeLicenseAccepted(Activity activity, boolean isAccepted) {
        SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(LICENSE_PREFERENCE_TAG, isAccepted);
        editor.commit();
    }

    /**
     *
     * @param activity Activity - interested activity
     * @return String the user name of user who logged in at last
     */
    public static String readLastLoggedUser(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREFS_LAST_LOGGED_USER, 0);
        String lastLoggedUser = settings.getString(PREFS_LAST_LOGGED_USER, null);
        return lastLoggedUser;
    }

    /**
     *
     * @param activity Activity - interested activity
     * @param userName String - the user name who logged in last
     */
    public static void writeLastLoggedUser(Activity activity, String userName) {
        SharedPreferences settings = activity.getSharedPreferences(PREFS_LAST_LOGGED_USER, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFS_LAST_LOGGED_USER, userName);
        editor.commit();
    }

    /**
     * @param activity Activity
     * @param path String - the pat which want to store
     */
    public static void writeLogFilePath(Activity activity, String path) {
        SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(LOG_FILE_PATH_TAG, path);
        editor.commit();
    }

    /**
     * @param activity
     * @return String the stored path if exists else null
     */
    public static String readLogFilePath(Activity activity) {
        SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, 0);
        String path = settings.getString(LOG_FILE_PATH_TAG, null);
        return path;
    }


    /**
     *
     * @param withSerial Boolean - extract with serial number
     * @return String - device name with serial number
     */
    @SuppressLint("NewApi")
    public static String extractDeviceName(boolean withSerial) {
        String deviceName = "";
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            deviceName = deviceNameToCapitalize(model);
        }
        else {
            deviceName = deviceNameToCapitalize(manufacturer) + " " + model;
        }
        if (withSerial) {
            if (android.os.Build.VERSION.SDK_INT >= 9) {
                deviceName += " (sn:" +Build.SERIAL+")";
            }
        }
        return deviceName;
    }

    /**
     *
     * @param deviceName String - device name for capitalize
     * @return String - capitalized device name
     */
    private static String deviceNameToCapitalize(String deviceName) {
        if (deviceName == null || deviceName.length() == 0) {
            return "";
        }
        char first = deviceName.charAt(0);
        if (Character.isUpperCase(first)) {
            return deviceName;
        }
        else {
            return Character.toUpperCase(first) + deviceName.substring(1);
        }
    }

    /**
     *
     * @return String - Android version in text
     */
    public static String extractAndroidVersion() {
        String ver = null;
        StringBuilder builder = new StringBuilder();
        builder.append("Android ").append(Build.VERSION.RELEASE);

        Field[] fields = Build.VERSION_CODES.class.getFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            int fieldValue = -1;

            try {
                fieldValue = field.getInt(new Object());
            }
            catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            catch (NullPointerException e) {
                e.printStackTrace();
            }

            if (fieldValue == Build.VERSION.SDK_INT) {
                builder.append(" ");
                builder.append(fieldName);
                builder.append(" ");
                builder.append("SDK#").append(fieldValue);
            }
        }

        ver = builder.toString();
        builder = null;
        return ver;
    }

    /**
     * @param activity Activity
     * @param packageName String - desired package name
     * @return boolean - true if installed else not
     */
    public static boolean isAppInstalled(Activity activity, String packageName) {
        PackageManager packageManager = activity.getPackageManager();
        boolean isInstalled = false;
        try {
            packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            isInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            isInstalled = false;
        }
        return isInstalled;
    }

}
