package btchat.app.mobile.com.brainteam.btchat.resthandling;

/**
 * Created by lkovari on 2015.02.22..
 */
public class RESTPathDefinitions {
    // does not fit to restful rules
    public static final String URL_PATH_LOGIN = "/login";
    public static final String URL_PATH_USERINFO = "/api/userinfo";
    public static final String URL_PATH_USERS = "/users";

}
