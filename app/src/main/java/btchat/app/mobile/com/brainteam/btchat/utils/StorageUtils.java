package btchat.app.mobile.com.brainteam.btchat.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.widget.Toast;

import btchat.app.mobile.com.brainteam.btchat.R;
import btchat.app.mobile.com.brainteam.btchat.logger.CommonLogger;
import btchat.app.mobile.com.brainteam.btchat.preference.BTCSettings;


/**
 * Created by lkovari on 2015.02.08..
 */
public class StorageUtils {
    private static String fileExt = null;
    private static final int TIMEOUT_CONNECTION = 5000;
    private static final int TIMEOUT_SOCKET = 30000;

    public static StringBuffer log = new StringBuffer();
    private static int cnt = 0;
    public static boolean isStoreDebugLog = false;

    public static void debug(String text) {
        if (isStoreDebugLog) {
            cnt++;
            log.append("> " + cnt + ". " + text + "\n\r");
        }
    }


    /**
     * #6 11/26/2013
     * @param context Context - current context
     * @param status String - status of device
     * @param isShowMessage boolean - true if need to show message
     */
    public static void checkMediaState(Context context, String status, boolean isShowMessage) {
        String mess = null;
        if (Environment.MEDIA_MOUNTED.equals(status)) {
            // nothing to do
        }
        else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(status)) {
            if (isShowMessage)
                mess =  context.getResources().getString(R.string.error_externalstorage_mounted_read_only);;
        }
        else if (Environment.MEDIA_REMOVED.equals(status)) {
            if (isShowMessage)
                mess = context.getResources().getString(R.string.error_externalstorage_not_present);
        }
        else if (Environment.MEDIA_UNMOUNTABLE.equals(status)){
            if (isShowMessage)
                mess = context.getResources().getString(R.string.error_externalstorage_media_unmountable);
        }
        else if (Environment.MEDIA_BAD_REMOVAL.equals(status)){
            mess = context.getResources().getString(R.string.error_externalstorage_media_bad_removal);
        }
        else if (Environment.MEDIA_NOFS.equals(status)){
            mess = context.getResources().getString(R.string.error_externalstorage_nofs);
        }
        else if (Environment.MEDIA_UNMOUNTED.equals(status)){
            mess = context.getResources().getString(R.string.error_externalstorage_media_unmounted);
        }
        if (isShowMessage && (mess != null))
            Toast.makeText(context, mess, Toast.LENGTH_LONG).show();
    }

    /**
     * @param context Context - a current context
     * @param isShowMessage boolean - true if need to show message
     * @return String - return the status
     */
    public static String checkExternalStorageAvailable(Context context, boolean isShowMessage) {
        String mess = null;
        if (context == null)
            isShowMessage = false;
        String status = Environment.getExternalStorageState();
        checkMediaState(context, status, isShowMessage);
        return status;
    }



    /**
     * @param folderName String - the name of folder
     * @return boolean - true if success
     */
    public static boolean createFolderOnSDCard(String folderName) {
        boolean isSuccess = false;
        try {
            // #5 10/06/2013
            String state = checkExternalStorageAvailable(null, false);
            if (state.equals(Environment.MEDIA_MOUNTED)) {
                File root = Environment.getExternalStorageDirectory();
                if (root.canWrite()) {
                    File folder = new File(root.getPath() + "/"  + folderName + "/");
                    if (!folder.exists()) {
                        isSuccess = folder.mkdirs();
                    }
                    else {
                        isSuccess = true;
                    }
                }
            }
        }
        catch (Exception e) {
            isSuccess = false;
        }
        return isSuccess;
    }

    /**
     * #5 10/06/2013
     * @param src
     * @param dst
     * @throws IOException
     */
    public static void copyFile(File src, File dst) throws IOException {
        BufferedInputStream buffInStream = null;
        try {
            buffInStream = new BufferedInputStream(new FileInputStream(src));
            BufferedOutputStream buffOutStream = null;
            try {
                buffOutStream = new BufferedOutputStream(new FileOutputStream(dst, false));
                byte[] buffer = new byte[1024];
                int len;
                while ((len = buffInStream.read(buffer)) > 0) {
                    buffOutStream.write(buffer, 0, len);
                }
            }
            finally {
                if (buffOutStream != null) {
                    buffOutStream.close();
                }
            }
        } finally {
            if (buffInStream != null) {
                buffInStream.close();
            }
        }
    }

    /**
     * 12/29/2012
     * @param directory
     * @param ext
     * @return
     */
    public static List<File> findFiles(String directory, String ext) {
        List<File> foundFiles = new ArrayList<File>();
        fileExt = ext;
        File dir = new File(directory);
        File[] fileMatches = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name)  {
                return name.endsWith(fileExt);
            }
        });
        // add to list
        if (fileMatches != null) {
            for (File f : fileMatches) {
                foundFiles.add(f);
            }
            // order
            Collections.sort(foundFiles, new Comparator<File>() {
                @Override
                public int compare(File lhs, File rhs) {
                    int res = lhs.getName().compareTo(rhs.getName());
                    return res;
                }

            });
        }
        return foundFiles;
    }

    /**
     * 01/13/2013
     * @param path
     * @return
     */
    public static List<File> findFile(String path) {
        List<File> foundFiles = new ArrayList<File>();
        File file = new File(path);
        if (file.exists()) {
            foundFiles.add(file);
        }
        return foundFiles;
    }

    /**
     *
     * @param fileName
     * @return
     */
    public static boolean isFileExists(String fileName) {
        boolean isFileExists = false;
        // #5 10/01/2013
        String state = checkExternalStorageAvailable(null,false);
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File root = Environment.getExternalStorageDirectory();
            if (root.canRead()) {
                File file = new File(root, fileName);
                isFileExists = file.exists();
                file = null;
            }
        }
        return isFileExists;
    }

    /**
     * 10/29/2012
     * @param context
     * @param fileName
     * @param content
     */
    public static String createFileForStringBuffer(Context context, String fileName, StringBuffer content) {
        // 11/25/2012
        String filePath = null;
        try {
            // #5 10/01/2013
            String state = checkExternalStorageAvailable(context, true);
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                File root = Environment.getExternalStorageDirectory();
                if (root.canWrite()) {
                    // #5 10/06/2013
                    File file = null;
                    File folderDir = new File(Environment.getExternalStorageDirectory() + "/" + BTCSettings.BTC_LOG_FOLDER);
                    if (!folderDir.exists()) {
                        if (folderDir.mkdir()) {
                            file = new File(folderDir.getAbsolutePath() + "/" + fileName);
                        }
                        else {
                            throw new Exception("Directory can\'t created " + folderDir.getAbsolutePath());
                        }
                    }
                    else {
                        file = new File(folderDir.getAbsolutePath() + "/" + fileName);
                    }
                    FileWriter fileWriter = new FileWriter(file);
                    BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
                    bufferWriter.write(content.toString());
                    bufferWriter.flush();
                    bufferWriter.close();
                    // 11/25/2012
                    filePath = file.getAbsolutePath();
                }
                else {
                    Toast.makeText(context, "ERROR! SDCard media is read only!", Toast.LENGTH_LONG).show();
                }
            }
            else {
                Toast.makeText(context, "ERROR! SDCard media is not mounted!", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            CommonLogger.log(Level.SEVERE, e.getMessage());
            Toast.makeText(context, "ERROR in createFile " + e.getMessage(), Toast.LENGTH_LONG).show();
            // 11/25/2012
            filePath = null;
        }
        // 11/25/2012
        return filePath;
    }

    /**
     * #1/10/2014 #10 v1.02.7
     * @param context Context
     * @return String - absolute path
     */
    public static String composeAbsolutePath(Context context) {
        String absolutePath = null;
        try {
            // #5 10/01/2013
            String state = checkExternalStorageAvailable(context, true);
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                File root = Environment.getExternalStorageDirectory();
                if (root.canWrite()) {
                    File folderDir = new File(Environment.getExternalStorageDirectory() + "/" + BTCSettings.BTC_LOG_FOLDER);
                    if (!folderDir.exists()) {
                        if (folderDir.mkdir()) {
                            absolutePath = folderDir.getAbsolutePath();
                        }
                        else {
                            throw new Exception("Directory can\'t created " + folderDir.getAbsolutePath());
                        }
                    }
                    else {
                        absolutePath =  folderDir.getAbsolutePath();
                    }
                }
                else {
                    Toast.makeText(context, "ERROR! SDCard media is read only!", Toast.LENGTH_LONG).show();
                }
            }
            else {
                Toast.makeText(context, "ERROR! SDCard media is not mounted!", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            CommonLogger.log(Level.SEVERE, e.getMessage());
            Toast.makeText(context, "ERROR in createFile " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
        // 11/25/2012
        return absolutePath;
    }

    /**
     * #1/10/2014 #10 v1.02.7
     * @param context Contect
     * @param path String - path of log files
     * @param fileName String - log file name
     * @param content StringBuffer - which want to store
     * @return String - file path
     */
    public static String createFileForStringBufferWithPath(Context context, String path, String fileName, StringBuffer content) {
        // 11/25/2012
        String filePath = null;
        try {
            File folderDir = new File(path);
            if (folderDir.exists()) {
                File file = new File(path + "/" + fileName);
                FileWriter fileWriter = new FileWriter(file);
                BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
                bufferWriter.write(content.toString());
                bufferWriter.flush();
                bufferWriter.close();
                // 11/25/2012
                filePath = file.getAbsolutePath();
            }
        } catch (Exception e) {
            e.printStackTrace();
            CommonLogger.log(Level.SEVERE, e.getMessage());
            Toast.makeText(context, "ERROR in createFile " + e.getMessage(), Toast.LENGTH_LONG).show();
            // 11/25/2012
            filePath = null;
        }
        // 11/25/2012
        return filePath;
    }
    /**
     *
     * @param context
     * @param fileName
     * @param content
     * @return
     */
    public static String createFileForRoutePlan(Context context, String fileName, StringBuffer content) {
        String timeStamp = CommonLogger.calculateFileNameStamp();
        fileName += timeStamp + ".txt";
        return createFileForStringBuffer(context, fileName, content);
    }

    /**
     * 10/29/2012
     * @param context
     * @param fileName
     * @param content
     */
    public static void createFileForString(Context context, String fileName, String content) {
        File storage = null;
        try {
            // #5 10/01/2013
            String state = checkExternalStorageAvailable(context, true);
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                storage = Environment.getExternalStorageDirectory();
                if (storage.canWrite()){
                    File file = new File(storage, fileName);
                    FileWriter fileWriter = new FileWriter(file);
                    BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
                    bufferWriter.write(content);
                    bufferWriter.flush();
                    bufferWriter.close();
                }
            }
            else {
                // #5 10/06/2013 try to use internal storage
                storage = context.getFilesDir();
                if (storage.canWrite()) {
                    File file = new File(storage, fileName);
                    FileWriter fileWriter = new FileWriter(file);
                    BufferedWriter bufferWriter = new BufferedWriter(fileWriter);
                    bufferWriter.write(content);
                    bufferWriter.flush();
                    bufferWriter.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            CommonLogger.log(Level.SEVERE, e.getMessage());
            if (context != null)
                Toast.makeText(context, "ERROR when createFile " + e.getMessage() + "\n" + storage.getAbsolutePath(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * #8 v1.02.502/15/2014
     * @param context
     * @param imei
     * @return
     */
    public static String saveImeiToFile(Context context, String imei) {
        String savePath = null;
        String fileName = CommonLogger.BASE_FILE_NAME + Build.MODEL +"_IMEI.txt";
        StringBuffer text = new StringBuffer();
        // device name and how many sensors are available
        text.append(Utils.extractDeviceName(true) + "\n\r ");
        // android version
        String androidVer = Utils.extractAndroidVersion();
        if (!androidVer.equals(""))
            text.append(Utils.extractAndroidVersion() + " \n\r ");
        text.append(" \n\r ");

        try {
            String imeiMd5 = ConnectionUtils.md5(imei);
            text.append("IMEI " + imei);
            text.append(" \n\r ");
            text.append("IMEI Md5 " + imeiMd5);
        }
        catch (Exception e) {
            text.append("IMEI Md5 -");
        }
        text.append(" \n\r ");
        if (context != null) {
            savePath = StorageUtils.createFileForStringBuffer(context, fileName, text);
            if (savePath != null) {
                Toast.makeText(context, "IME SAVEd to : "+savePath, Toast.LENGTH_LONG).show();
            }
        }
        text = null;
        return savePath;
    }

    /**
     * @param activity Activity
     * @param ext String - file extension with dot
     * @return List<String> - list of files which found with the specified ext
     */
    public static List<String> loadLogFilesPath(Activity activity, String ext) {
        List<String> logFilePath = null;
        String storedPath = Utils.readLogFilePath(activity);
        if ((storedPath == null) || ((storedPath != null) && (storedPath.trim() == ""))) {
            Toast.makeText(activity.getApplicationContext(), "No log file stored!", Toast.LENGTH_LONG).show();
        }
        else {
            logFilePath = new ArrayList<String>();
            File dir = new File(storedPath);
            if (dir.exists()) {
                File[] files = dir.listFiles();
                for (File f : files) {
                    if (f.getName().contains(ext)) {
                        logFilePath.add(f.getName());
                    }
                }
            }
        }
        return logFilePath;
    }



}
