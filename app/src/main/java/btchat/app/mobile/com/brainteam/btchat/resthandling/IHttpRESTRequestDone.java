package btchat.app.mobile.com.brainteam.btchat.resthandling;

/**
 * Created by lkovari on 2015.02.17..
 */
public interface IHttpRESTRequestDone {
    String requestDone(String response);
}
