package btchat.app.mobile.com.brainteam.btchat.json;

/**
 * Created by lkovari on 2015.02.15..
 */
public class UserJSonWrapper extends BaseJSonWrapper {
    private String user_id;
    private String user_name;
    private String hashed_pwd;
    private String salt;

    /**
     *
     * @param user_id String - GUID ID of user
     * @param user_name String - name of user
     * @param hashed_pwd String - hashed password
     * @param salt String - salt
     */
    public UserJSonWrapper(String user_id, String user_name, String hashed_pwd, String salt) {
        this.user_id = user_id;
        this.user_name = user_name;
        this.hashed_pwd = hashed_pwd;
        this.salt = salt;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getHashed_pwd() {
        return hashed_pwd;
    }

    public void setHashed_pwd(String hashed_pwd) {
        this.hashed_pwd = hashed_pwd;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Override
    public String toJson() {
        return GSON.toJson(this, UserJSonWrapper.class);
    }
}
