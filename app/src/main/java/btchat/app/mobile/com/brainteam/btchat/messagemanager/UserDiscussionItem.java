package btchat.app.mobile.com.brainteam.btchat.messagemanager;

import java.util.ArrayList;
import java.util.List;

import btchat.app.mobile.com.brainteam.btchat.datawrappers.UserData;

/**
 * Created by lkovari on 2015.02.23..
 */
public class UserDiscussionItem {
    private UserData participantUserData;
    private List<UserMessageItem> conversation = new ArrayList<UserMessageItem>();

    public UserDiscussionItem(UserData participantUserData, List<UserMessageItem> conversation) {
        this.participantUserData = participantUserData;
        this.conversation = conversation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserDiscussionItem)) return false;

        UserDiscussionItem that = (UserDiscussionItem) o;

        if (!participantUserData.equals(that.participantUserData)) return false;

        return true;
    }

    @Override
    public String toString() {
        return "UserDiscussionItem{" +
                "participantUserData.userName=" + participantUserData.getUserName() +
                '}';
    }

    @Override
    public int hashCode() {
        return participantUserData.hashCode();
    }

    public UserData getParticipantUserData() {
        return participantUserData;
    }

    public void setParticipantUserData(UserData participantUserData) {
        this.participantUserData = participantUserData;
    }

    public List<UserMessageItem> getConversation() {
        return conversation;
    }

    public void setConversation(List<UserMessageItem> conversation) {
        this.conversation = conversation;
    }
}
