package btchat.app.mobile.com.brainteam.btchat.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import btchat.app.mobile.com.brainteam.btchat.datawrappers.UserData;

/**
 * Created by lkovari on 2015.02.08..
 */
public class BTCSettings {

    public static String BTC_LOG_FOLDER = "btclogs";

    public static String PHONE_IMEI = null;
    public static boolean IS_EXTRA_FUNCTIONS_ENABLED = false;

    // Notification Settings related
    public static boolean  IS_AUDIO_NOTIFICATION_ENABLED = true;

    // Connection related settings
    public static String HOST_ADDRESS = "";
    public static int HOST_PORT = 3000;
    public static String USER_NAME = null;
    public static String USER_PASSWORD = null;

    public static UserData currentUserData;

    /**
     *
     * @param context Context - the current context
     */
    public static void loadPreferences(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        //Connection related
        String hostURI = sharedPreferences.getString("key_host_uri", "10.0.2.2");
        BTCSettings.HOST_ADDRESS = hostURI;

        String hostPort = sharedPreferences.getString("key_host_port", "3000");
        BTCSettings.HOST_PORT = Integer.parseInt(hostPort);

        String userName = sharedPreferences.getString("key_user_name", null);
        BTCSettings.USER_NAME = userName;

        String userPassword = sharedPreferences.getString("key_user_password", null);
        BTCSettings.USER_PASSWORD = userPassword;

        // Notification related
        boolean isAutdioNotificationEnabled = sharedPreferences.getBoolean("key_audio_notification", true);
        BTCSettings.IS_AUDIO_NOTIFICATION_ENABLED = isAutdioNotificationEnabled;


    }

}
