package btchat.app.mobile.com.brainteam.btchat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by lkovari on 2015.02.28..
 */
public class ShowLoginDialog {
    //private static Dialog loginDialog = null;
    private static boolean isShowUserName = false;
    //private static Activity parentActivity = null;
    private static String USER_NAME = null;
    private static String[] un_pw = null;

    /**
     *
     * @param activity Activity - parent activity of dialog
     * @param userName String - contains user name if it is available (no preference data and last logged user name available)
     * @return String[] - first element contains user name, second element contains password
     */
    public static String[] showLoginDialog(Activity activity, String userName) {
        final Activity parentActivity = activity;
        ShowLoginDialog.isShowUserName = (userName == null);
        /*
        AlertDialog.Builder loginDialogBuilder = new AlertDialog.Builder(activity);
        final AlertDialog loginDialog = loginDialogBuilder.create();
        loginDialog.setContentView(R.layout.dialog_login);
        */

        LayoutInflater inflater = LayoutInflater.from(activity);
        View dialogview = inflater.inflate(R.layout.dialog_login, null);
        AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(activity);
        dialogbuilder.setView(dialogview);
        final AlertDialog loginDialog = dialogbuilder.create();

        Button loginButton = (Button)loginDialog.findViewById(R.id.loginLoginButton);
        Button cancelButton = (Button)loginDialog.findViewById(R.id.loginCancelButton);

        final EditText userNameEditText = (EditText)loginDialog.findViewById(R.id.loginUserNameEditText);
        final EditText passwordEditText = (EditText)loginDialog.findViewById(R.id.loginPasswordEditText);

        loginDialog.setTitle(activity.getString(R.string.login_dialog_login_title) + " " + userName);

        un_pw = new String[2];

        // need to disable user name EditText?
        if (!isShowUserName) {
            // set user name to invisible
            userNameEditText.setEnabled(false);
            userNameEditText.setVisibility(View.INVISIBLE);
        }
        else {
            // set user name to invisible
            userNameEditText.setEnabled(true);
            userNameEditText.setVisibility(View.VISIBLE);
        }

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // if user name not needed to type set default
                un_pw[0] = ShowLoginDialog.USER_NAME;

                if (ShowLoginDialog.isShowUserName) {
                    if ((userNameEditText.getText().toString() != null) && (!userNameEditText.getText().toString().trim().isEmpty())) {
                        // set username into result
                        un_pw[0] = userNameEditText.getText().toString().trim();
                    } else {
                        Toast.makeText(parentActivity.getApplicationContext(), R.string.login_dialog_error_username_empty, Toast.LENGTH_LONG).show();
                    }
                }
                // set password into result
                if ((passwordEditText.getText().toString() != null) && (!passwordEditText.getText().toString().trim().isEmpty())) {
                    un_pw[1] = passwordEditText.getText().toString().trim();
                    loginDialog.dismiss();
                }
                else {
                    Toast.makeText(parentActivity.getApplicationContext(), R.string.login_dialog_error_password_empty , Toast.LENGTH_LONG).show();
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                un_pw = null;
                loginDialog.dismiss();
            }
        });

        // show dialog
        loginDialog.setCancelable(false);
        loginDialog.show();

        return un_pw;
    }
}
