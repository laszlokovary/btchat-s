package btchat.app.mobile.com.brainteam.btchat.displaymanager;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.text.Html;
import android.text.Spanned;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import btchat.app.mobile.com.brainteam.btchat.R;

/**
 * Created by lkovari on 2015.02.08..
 */
public class ChatMessageListAdapter extends ArrayAdapter<ChatMessage> {
    private boolean IS_USING_HTML = true;
    private TextView message;
    private TextView stamp;
    private ImageView picture;
    private LinearLayout chatMessageLayout = null;

    static class ChatMessageViewHolderItem {
        ImageView pictureleft;
        TextView message;
        ImageView pictureright;
    }

    public ChatMessageListAdapter(Context context, int listViewId, List<ChatMessage> messList) {
        super(context, listViewId, messList);


    }

    private Spanned constructHtmlMessage(Date timestamp, String mess, boolean isLeft) {
        String message = mess.replace("\n", "<br>");
        DateFormat dateForma = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, Locale.getDefault());
        String formattedDate = dateForma.format(timestamp);
        Spanned res;
        if (isLeft) {
            res = Html.fromHtml(mess + "<br><b><i><font color='#888888'><small><small>" + formattedDate + "</small></small></font></i></b>");
        }
        else {
            res = Html.fromHtml(mess+"<br><b><i><font color='#888888'><small><small>"+formattedDate+"</small></small></font></i></b>");
        }
        return res;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        ChatMessageViewHolderItem chatMessageViewHolderItem = null;


        if (rowView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            rowView = layoutInflater.inflate(R.layout.listitem_mess, parent, false);

            chatMessageViewHolderItem = new ChatMessageViewHolderItem();
            chatMessageViewHolderItem.message = (TextView) rowView.findViewById(R.id.textMessageTextView);
//            chatMessageViewHolderItem.stamp = (TextView) rowView.findViewById(R.id.messageSent);
            chatMessageViewHolderItem.pictureleft = (ImageView) rowView.findViewById(R.id.leftPictureImageView);
            chatMessageViewHolderItem.pictureright = (ImageView) rowView.findViewById(R.id.rightPictureImageView);

            rowView.setTag(chatMessageViewHolderItem);
        }

        chatMessageLayout = (LinearLayout) rowView.findViewById(R.id.chatMessageLayout);

        chatMessageViewHolderItem = (ChatMessageViewHolderItem) rowView.getTag();

        ChatMessage chatMessage = getItem(position);


        if (chatMessage != null) {

            LinearLayout.LayoutParams layoutParamsMessage = (LinearLayout.LayoutParams)chatMessageViewHolderItem.message.getLayoutParams();
//            RelativeLayout.LayoutParams layoutParamsStamp = (RelativeLayout.LayoutParams)chatMessageViewHolderItem.stamp.getLayoutParams();
            LinearLayout.LayoutParams layoutParamsPictureLeft = (LinearLayout.LayoutParams)chatMessageViewHolderItem.pictureleft.getLayoutParams();
            LinearLayout.LayoutParams layoutParamsPictureRight = (LinearLayout.LayoutParams)chatMessageViewHolderItem.pictureright.getLayoutParams();

            chatMessageViewHolderItem.message.setText(chatMessage.getMessage());
//            chatMessageViewHolderItem.stamp.setText(chatMessage.getTimeStamp().toString());

            switch (chatMessage.getMessageSideKind()) {
                case RIGHT: {
                    // align to right

                    chatMessageViewHolderItem.pictureleft.setImageBitmap(chatMessage.getFaceImage());

                    chatMessageViewHolderItem.message.setLayoutParams(layoutParamsMessage);
                    chatMessageViewHolderItem.message.setGravity(Gravity.RIGHT);

                    layoutParamsPictureRight.gravity = Gravity.RIGHT;
                    chatMessageViewHolderItem.pictureright.setLayoutParams(layoutParamsPictureRight);
                    chatMessageViewHolderItem.pictureright.setImageResource(R.drawable.np_face_right);
                    chatMessageViewHolderItem.pictureright.setVisibility(View.VISIBLE);
                    chatMessageViewHolderItem.pictureleft.setVisibility(View.INVISIBLE);
                    break;
                }
                case LEFT: {
                    // align message to left

                    chatMessageViewHolderItem.pictureright.setImageBitmap(chatMessage.getFaceImage());

                    chatMessageViewHolderItem.message.setLayoutParams(layoutParamsMessage);
                    chatMessageViewHolderItem.message.setGravity(Gravity.LEFT);
//                    chatMessageViewHolderItem.message.setVisibility(View.INVISIBLE);
                    layoutParamsPictureLeft.gravity = Gravity.LEFT;
                    chatMessageViewHolderItem.pictureleft.setLayoutParams(layoutParamsPictureLeft);
                    chatMessageViewHolderItem.pictureleft.setImageResource(R.drawable.np_face_left);
                    chatMessageViewHolderItem.pictureleft.setVisibility(View.VISIBLE);
                    chatMessageViewHolderItem.pictureright.setVisibility(View.INVISIBLE);
                    break;
                }
            }

            if (IS_USING_HTML) {
                Spanned spannedContent = constructHtmlMessage(chatMessage.getTimeStamp(), chatMessage.getMessage(), chatMessage.getMessageSideKind().equals(MessageSideKind.LEFT));
                chatMessageViewHolderItem.message.setText(spannedContent);
            }
            else {
                // set formatted date into message
                DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, Locale.getDefault());
                String formattedDate = dateFormat.format(chatMessage.getTimeStamp());
                chatMessageViewHolderItem.message.setText(chatMessage.getMessage() + "\n" + formattedDate);
            }


            // background (ninepatch) for text message
            chatMessageViewHolderItem.message.setBackgroundResource(chatMessage.getMessageSideKind().equals(MessageSideKind.LEFT) ? R.drawable.np_ls : R.drawable.np_rs);

            // set message gravity
            chatMessageLayout.setGravity(chatMessage.getMessageSideKind().equals(MessageSideKind.LEFT) ? Gravity.LEFT : Gravity.RIGHT);
        }




        return rowView;
    }
}
