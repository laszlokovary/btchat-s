package btchat.app.mobile.com.brainteam.btchat;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import btchat.app.mobile.com.brainteam.btchat.utils.Utils;

/**
 * Created by lkovari on 2015.02.08..
 */
public class License extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_license);

        // fix orientation
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Restore preferences
        boolean isLicenseAccepted = Utils.readLicenseAccepted(this);

        if (isLicenseAccepted) {
            Intent myIntent = new Intent(this, Chat.class);
            // code 1965 means this is main need to exit
            startActivityForResult(myIntent, 1965);
        }
        else {

            final Button acceptbutton = (Button) findViewById(R.id.accept_btn);
            acceptbutton.setTextColor(Color.GREEN);
            acceptbutton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // write preference
                    Utils.writeLicenseAccepted(License.this, true);
                    Intent myIntent = new Intent(v.getContext(), Chat.class);
                    startActivityForResult(myIntent, 1965);
                }
            });

            final Button refuseButton = (Button) findViewById(R.id.refuse_btn);
            refuseButton.setTextColor(Color.RED);
            refuseButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    finish();
                }
            });
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1965) {
            if (resultCode == Activity.RESULT_OK) {
                boolean isExit = data.getExtras().getBoolean("IS_EXIT");
                if (isExit)
                    finish();
            }
        }
    }
}
