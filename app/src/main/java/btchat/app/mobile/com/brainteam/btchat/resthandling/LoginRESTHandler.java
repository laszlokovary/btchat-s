package btchat.app.mobile.com.brainteam.btchat.resthandling;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lkovari on 2/16/2015.
 */
public class LoginRESTHandler extends HttpPostRESTHandler {
    private String userName;
    private String password;
    private String userId;
    private String salt;


    /**
     *
     * @param hostAddress
     * @param port
     * @param requestMode
     * @param un
     * @param pw
     */
    public LoginRESTHandler(String hostAddress, int port, RequestMode requestMode, IHttpRESTRequestDone httpRESTRequestDone, String un, String pw) {
        super(hostAddress, port, requestMode, httpRESTRequestDone);
        this.userName = un;
        this.password = pw;
    }

    /**
     *
     * @param params
     * @return
     */
    @Override
    protected List<NameValuePair> createParams(String... params) {
        String userName = params[0];
        String password = params[1];
        List<NameValuePair> parameters = new ArrayList<NameValuePair>();
        NameValuePair userNameParam = new BasicNameValuePair("username", userName);
        parameters.add(userNameParam);
        NameValuePair pwParam = new BasicNameValuePair("password", password);
        parameters.add(pwParam);
        return parameters;
    }


    /**
     *
     * @param parameters
     * @param url_path
     * @return
     */
    @Override
    protected String doIt(List<NameValuePair> parameters, String url_path) {
        List<NameValuePair> currentParameters = null;
        String currentUrlPath = null;

        if (parameters != null)
            currentParameters = parameters;
        else
            currentParameters = createParams(this.userName, this.password);

        if (url_path != null)
            currentUrlPath = url_path;
        else
            currentUrlPath = RESTPathDefinitions.URL_PATH_LOGIN;

        return super.doIt(currentParameters, currentUrlPath);
    }


    @Override
    protected void onPostExecute(String result) {
        String responseText = result;
        if (this.httpRESTRequestDone != null) {
            this.httpRESTRequestDone.requestDone(responseText);
        }
        super.onPostExecute(result);
    }

    @Override
    protected String doInBackground(String... params) {
        String response = null;
        BaseRESTHandler baseRESTHandler = null;
        try {
            response = doIt(null, null);
        } catch (Exception e) {
            response = null;
        }
        finally {
            baseRESTHandler = null;
        }
        return response;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getUserId() {
        return userId;
    }

    public String getSalt() {
        return salt;
    }
}
