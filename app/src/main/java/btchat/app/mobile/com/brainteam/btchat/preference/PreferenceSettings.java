package btchat.app.mobile.com.brainteam.btchat.preference;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.TextUtils;
import android.widget.Toast;

import btchat.app.mobile.com.brainteam.btchat.R;

import java.util.List;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p/>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class PreferenceSettings extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    /**
     * Determines whether to always show the simplified settings UI, where
     * settings are presented in a single list. When false, settings are shown
     * as a master/detail two-pane view on tablets. When true, a single pane is
     * shown on tablets.
     */
    private static final boolean ALWAYS_SIMPLE_PREFS = false;

    private boolean isChanged = false;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setTheme(android.R.style.Theme_Black);
        setupSimplePreferencesScreen();
    }

    /**
     * Shows the simplified settings UI if the device configuration if the
     * device configuration dictates that a simplified, single-pane UI should be
     * shown.
     */
    private void setupSimplePreferencesScreen() {
        if (!isSimplePreferences(this)) {
            return;
        }

        // In the simplified UI, fragments are not used at all and we instead
        // use the older PreferenceActivity APIs.

        PreferenceManager.setDefaultValues(getApplicationContext(), R.xml.pref_general, false);

        SharedPreferences preferencesSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        preferencesSettings.registerOnSharedPreferenceChangeListener(this);

        // Add 'general' preferences.
        addPreferencesFromResource(R.xml.pref_general);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this) && !isSimplePreferences(this);
    }

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Determines whether the simplified settings UI should be shown. This is
     * true if this is forced via {@link #ALWAYS_SIMPLE_PREFS}, or the device
     * doesn't have newer APIs like {@link PreferenceFragment}, or the device
     * doesn't have an extra-large screen. In these cases, a single-pane
     * "simplified" settings UI should be shown.
     */
    private static boolean isSimplePreferences(Context context) {
        return ALWAYS_SIMPLE_PREFS || Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB || !isXLargeTablet(context);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("key_host_uri")) {
            String dataProvider = sharedPreferences.getString("key_host_uri", "10.0.2.2");
            BTCSettings.HOST_ADDRESS = dataProvider;
        }
        else if (key.equals("key_host_port")) {
            String hostPort = sharedPreferences.getString("key_host_port", "3000");
            BTCSettings.HOST_PORT = Integer.parseInt(hostPort);
        }
        else if (key.equals("key_user_name")) {
            String userName = sharedPreferences.getString("key_user_name", null);
            BTCSettings.USER_NAME = userName;

        }
        else if (key.equals("key_user_password")) {
            String userPassword = sharedPreferences.getString("key_user_password", null);
            BTCSettings.USER_PASSWORD = userPassword;
        }
        else if (key.equals("key_audio_notification")) {
            Boolean isNotificationEnabled = sharedPreferences.getBoolean("key_audio_notification", true);
            BTCSettings.IS_AUDIO_NOTIFICATION_ENABLED = isNotificationEnabled;
        }
        isChanged = true;
    }

    @Override
    protected void onStop() {
        if (isChanged) {
            Toast.makeText(PreferenceSettings.this, R.string.settings_take_effect, Toast.LENGTH_LONG).show();
        }
        super.onStop();
    }

}
